# Image for WE WEB
FROM amazonlinux:1
LABEL maintainer="nguyentruonghuy93@gmail.com"
EXPOSE 9007 80 443
WORKDIR /root

# Install Dependiences of Nginx and Php7.2.x 
RUN yum -y install wget \ 
  epel-release \ 
  git \
  nano \
  autoconf \
  dos2unix  \ 
  nginx \
  GeoIP \
  GeoIP-devel \
  GeoIP-data \
  curl-devel \
  curl \
  libjpeg-devel \
  libpng-devel \
  freetype-devel \
  gd \
  gd-devel \
  rpm \
  rpm-devel \
  libxml2-devel \
  libxslt-devel \
  gcc \
  gcc-c++ \
  openssl-devel \
  libicu-devel \
  mysql-devel \
  libmcrypt-devel \
  libmhash-devel \
  libXpm \
  libxml2 \
  libXpm-devel \
  php-xml \
  perl-Image-Xpm.noarch \
  libtool-ltdl-devel \
  t1lib \
  t1lib-devel \
  freetype-devel \
  python-setuptools \
  libvpx \
  libvpx-devel \
  gdbm-devel \
  libtool \
  cmake \
  libssh-devel \
  xmlto \
  doxygen

# Configure Configs and Install PHP7.2
RUN mkdir -p /etc/nginx/conf.d/ssl \
  && mkdir -p /etc/nginx/conf.d/vhosts \
  && mkdir -p /usr/share/nginx/html \
  && mkdir -p /var/www/projects \
  && touch /usr/share/nginx/html/index.php \
  && echo "<?php echo phpinfo(); exit; ?>" >>  /usr/share/nginx/html/index.php \
  && chown -Rf nginx:nginx /usr/share/nginx/html \
  && wget http://sg2.php.net/distributions/php-7.2.21.tar.gz \
  && mkdir -p /usr/local/php7.2 \
  && tar zxvf php-7.2.21.tar.gz \
  && cd php-7.2.21 \
  && ./configure \
  --prefix=/usr/local/php7.2 \
  --enable-fpm \
  --enable-mbstring \
  --enable-libxml \
  --enable-zip \
  --enable-sockets \
  --enable-exif \
  --enable-ftp \
  --enable-gd-native-ttf \
  --enable-soap \
  --enable-bcmath \
  --enable-intl \
  --enable-calendar \
  --enable-pcntl \
  --with-openssl \
  --with-mysqli \
  --with-mysql-sock \
  --with-gd \
  --with-jpeg-dir=/usr/lib \
  --with-pdo-mysql \
  --with-libxml-dir=/usr/lib \
  --with-xsl \
  --with-mysqli=/usr/bin/mysql_config \
  --with-curl \
  --with-mcrypt \
  --with-zlib \
  --with-iconv \
  --with-gettext \
  --with-freetype-dir=/usr \
  --with-fpm-user=nginx \
  --with-fpm-group=nginx \
  --with-libdir=lib64 \
  --with-gdbm \
  && make \
  && make install \
  && mkdir -p /usr/local/php7.2/logs \
  && cp /usr/local/php7.2/etc/php-fpm.d/www.conf.default /usr/local/php7.2/etc/php-fpm.d/www.conf \
  && cp /usr/local/php7.2/etc/php-fpm.conf.default /usr/local/php7.2/etc/php-fpm.conf \
  && ln -s /usr/local/php7.2/etc/php-fpm.conf /etc/php.conf \
  && ln -s /usr/local/php7.2/bin/php /usr/sbin/php \
  && ln -s /usr/local/php7.2/bin/phpize /usr/sbin/phpize \
  && ln -s /usr/local/php7.2/bin/pecl /usr/sbin/pecl \
  && ln -s /usr/local/php7.2/bin/php-config /usr/sbin/php-config \
  && ln -s /usr/local/php7.2/bin/pear /usr/sbin/pear 

# Copy base config files
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/php-fpm	/etc/init.d/php-fpm 
COPY config/www.conf /usr/local/php7.2/etc/php-fpm.d/www.conf
COPY config/php.ini /usr/local/php7.2/lib/php.ini
COPY config/mysql_connector.sh /etc/mysql_connector.sh

# Install Composer & Doctrine-migrations
RUN curl -sS https://getcomposer.org/installer | php \
  && mv composer.phar /usr/bin/composer \
  && chmod +x /usr/bin/composer \
  && wget https://github.com/doctrine/migrations/releases/download/v1.0.0/doctrine-migrations.phar \
  && cp doctrine-migrations.phar /usr/bin/doctrine-migrations \
  && chmod 0655 /usr/bin/doctrine-migrations

# Install WP-CLI
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
  && chmod +x wp-cli.phar \
  && mv wp-cli.phar /usr/local/bin/wp \
  && yum -y install mysql57 \
  && rpm -ivh https://packagecloud.io/phalcon/stable/packages/el/7/php71u-phalcon-debuginfo-3.4.4-1.ius.el7.x86_64.rpm/download.rpm

# Install RabbitMQ Client v0.8.0
RUN wget https://github.com/alanxz/rabbitmq-c/releases/download/v0.8.0/rabbitmq-c-0.8.0.tar.gz \
  && tar zxf rabbitmq-c-0.8.0.tar.gz \
  && cd rabbitmq-c-0.8.0 \
  && ./configure --prefix=/usr/local/rabbitmq-c-0.8.0 \
  && make \
  && make install 

# Install AMQP v1.8.0
RUN wget https://pecl.php.net/get/amqp-1.8.0.tgz \
  && tar zxf amqp-1.8.0.tgz \
  && cd amqp-1.8.0 \
  && phpize \
  && ./configure \
  --with-amqp \
  --with-librabbitmq-dir=/usr/local/rabbitmq-c-0.8.0 \
  && make \
  && make install \
  && echo "extension=amqp.so" >> /usr/local/php7.2/lib/php.ini

# Install Supervisor
RUN curl https://bootstrap.pypa.io/2.6/get-pip.py -o get-pip.py \
  && python get-pip.py \
  && pip install supervisor \
  && chmod +x /etc/init.d/php-fpm \
  && dos2unix /etc/init.d/php-fpm \
  && chmod +x /etc/mysql_connector.sh \
  && dos2unix /etc/mysql_connector.sh

# Install PHP-Phalcon
RUN git clone -b 3.4.x https://github.com/phalcon/cphalcon.git \ 
  && cd cphalcon/build/php7/64bits \
  && /usr/local/php7.2/bin/phpize \
  && ./configure --with-php-config=/usr/local/php7.2/bin/php-config \
  && make \
  && make install \
  && echo "extension=phalcon.so" >> /usr/local/php7.2/lib/php.ini 

# Install PHP-Xdebub, PHP-APCU
RUN pecl install xdebug \
  && echo "zend_extension=/usr/local/php7.2/lib/php/extensions/no-debug-non-zts-20170718/xdebug.so" >> /usr/local/php7.2/lib/php.ini \
  && echo "zend_extension=/usr/local/php7.2/lib/php/extensions/no-debug-non-zts-20170718/opcache.so" >> /usr/local/php7.2/lib/php.ini \
  && echo "xdebug.remote_enable=true" >> /usr/local/php7.2/lib/php.ini \
  && echo "php_flag[display_errors] = on" >> /usr/local/php7.2/etc/php-fpm.conf \
  && printf "\n" | pecl install apcu \
  && echo "extension=apcu.so" >> /usr/local/php7.2/lib/php.ini 

# Install PHP-Mongodb
RUN pecl install mongodb-1.5.3 \
  && echo "extension=mongodb.so" >> /usr/local/php7.2/lib/php.ini 

# Install PHP-Redis
RUN printf "\n" | pecl install redis-4.2.0 \
  && echo "extension=redis.so" >> /usr/local/php7.2/lib/php.ini 

COPY config/supervisord.conf /etc/supervisor/supervisord.conf
WORKDIR /var/www/projects
VOLUME [ "/var/www/projects" ]
CMD ["supervisord"]
